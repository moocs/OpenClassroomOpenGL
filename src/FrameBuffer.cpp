//
// Created by Antonin on 30/03/2018.
//

#include "FrameBuffer.h"

// TODO/title Constructeurs

FrameBuffer::FrameBuffer() : m_id(0), m_largeur(0), m_hauteur(0), m_colorBuffers(0), m_depthBufferID(0)
{

}

FrameBuffer::FrameBuffer(float m_largeur, float m_hauteur) : m_id(0), m_largeur(m_largeur), m_hauteur(m_hauteur),
                                                             m_colorBuffers(0), m_depthBufferID(0)
{

}

// TODO/title Destructeur

FrameBuffer::~FrameBuffer()
{

    glDeleteFramebuffers(1, &m_id);
    glDeleteRenderbuffers(1, &m_depthBufferID);

    m_colorBuffers.clear();
}

// TODO/title Autres méthodes

void FrameBuffer::creerRenderBuffer(GLuint &id, GLenum formatInterne)
{
    // Suppression du Render Buffer si il existe déjà
    if(glIsRenderbuffer(id) == GL_TRUE)
        glDeleteRenderbuffers(1, &id);

    // Création du Render Buffer
    glGenRenderbuffers(1, &id);

    // Verrouillage
    glBindRenderbuffer(GL_RENDERBUFFER, id);

    // Configuration
    glRenderbufferStorage(GL_RENDERBUFFER, formatInterne, m_largeur, m_hauteur);

    // Déverrouillage
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

bool FrameBuffer::charger()
{
    // Suppression du Frame Buffer si il existe déjà
    if(glIsFramebuffer(m_id) == GL_TRUE){
        glDeleteFramebuffers(1, &m_id);

        m_colorBuffers.clear();
    }

    // Création
    glGenFramebuffers(1, &m_id);

    // Verrouillage
    glBindFramebuffer(GL_FRAMEBUFFER, m_id);

    // TODO/title Création du Color Buffer
    Texture colorBuffer(m_largeur, m_hauteur, GL_RGBA, GL_RGBA, true);
    colorBuffer.chargerTextureVide();

    // Ajout à la liste des color buffers
    m_colorBuffers.push_back(colorBuffer);

    // TODO/title Création du Depth/Stencil Buffer
    creerRenderBuffer(m_depthBufferID, GL_DEPTH24_STENCIL8);

    // TODO/title Associations au FBO
    // Color Buffer
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_colorBuffers[0].getID(), 0);

    // Render Buffers
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_depthBufferID);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        // Libération des buffers
        glDeleteFramebuffers(1, &m_id);
        glDeleteRenderbuffers(1, &m_depthBufferID);

        m_colorBuffers.clear();

        // Affichage d'un message d'erreur
        std::cout << "Erreur : le FBO est mal construit" << std::endl;

        return false;
    }

    // Déverrouillage
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return true;


//    return configurationFBO();
}

bool FrameBuffer::configurationFBO()
{
    // Verrouillage
    glBindFramebuffer(GL_FRAMEBUFFER, m_id);

        // TODO/title Création du Color Buffer
        Texture colorBuffer(static_cast<int>(m_largeur), static_cast<int>(m_hauteur), GL_RGBA, GL_RGBA, true);
        colorBuffer.chargerTextureVide();

        // Ajout à la liste des color buffers
        m_colorBuffers.push_back(colorBuffer);

        // TODO/title Création du Depth/Stencil Buffer
        creerRenderBuffer(m_depthBufferID, GL_DEPTH24_STENCIL8);

        // TODO/title Associations au FBO
        // Color Buffer
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_colorBuffers[0].getID(), 0);

        // Render Buffers
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_depthBufferID);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            // Libération des buffers
            glDeleteFramebuffers(1, &m_id);
            glDeleteRenderbuffers(1, &m_depthBufferID);

            m_colorBuffers.clear();

            // Affichage d'un message d'erreur
            std::cout << "Erreur : le FBO est mal construit" << std::endl;

            return false;
        }

    // Déverrouillage
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    return true;
}

// TODO/title Getter & Setter

GLuint FrameBuffer::getID() const {
    return m_id;
}

GLuint FrameBuffer::getColorBufferID(unsigned int index) const {
    return m_colorBuffers[index].getID();
}

float FrameBuffer::getLargeur() const {
    return m_largeur;
}

float FrameBuffer::getHauteur() const {
    return m_hauteur;
}
