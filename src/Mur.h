//
// Created by Antonin on 27/03/2018.
//

#ifndef OPCL_CLION_MUR_H
#define OPCL_CLION_MUR_H

#include "Carre.h"
#include "Texture.h"
#include <string>

class Mur : public Carre{
public:

    Mur(int orientation, float taille, std::string vertexShader, std::string fragmentShader,
        std::string texture, int tailletexture);

    ~Mur();

    void afficher(glm::mat4 &projection, glm::mat4 &modelview);

    void charger() override;

private:
    Texture m_texture;

    float m_coordTexture[12];
    int m_tailleCoordTextureBytes;

};


#endif //OPCL_CLION_MUR_H
