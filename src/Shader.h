//
// Created by Antonin on 27/03/2018.
//

#ifndef OPCL_CLION_SHADER_H
#define OPCL_CLION_SHADER_H

#ifdef WIN32
#include <GL/glew.h>

#elif __APPlE__
#define GL3_PROTOYPES 1
#include <OPENGL/gl3.h>

#else
#define GL3_PROTOYPES 1
#include <GL3/gl3.h>

#endif

#include <iostream>
#include <string>
#include <fstream>
#include <utility>
#include <glm/glm.hpp>
#include <glm/detail/type_mat.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader {

public:

    Shader();

    Shader(std::string vertexSource, std::string fragmentSource);

    Shader(Shader const &shaderACopier);

    Shader &operator=(Shader const &shaderACopier);

    ~Shader();

    GLuint getProgramID() const;

    bool charger();

    void envoyerMat4(std::string nom, glm::mat4 matrice);

private:

    GLuint m_vertexID;
    GLuint m_fragmentID;
    GLuint m_programID;

    std::string m_vertexSource;
    std::string m_fragmentSource;

    bool compileShader(GLuint &shader, GLenum type, std::string const &fichierSource);

    bool isCompilationError(GLuint &shader);

    bool isLinkError(GLuint &programID);
};


#endif //OPCL_CLION_SHADER_H
