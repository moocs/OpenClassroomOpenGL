#ifndef DEF_SCENEOPENGL
#define DEF_SCENEOPENGL


// Includes

#include <GL/glew.h>


// Includes GLM

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>


// Autres includes

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <string>
#include "Texture.h"
#include "Shader.h"
#include "FrameBuffer.h"

#include "Cube.h"
#include "Caisse.h"

#include "Carre.h"
#include "Mur.h"

#include "Input.h"
#include "Camera.h"


// Classe

class SceneOpenGL
{
    public:

    SceneOpenGL(std::string titreFenetre, int largeurFenetre, int hauteurFenetre);
    ~SceneOpenGL();

    bool initialiserFenetre();
    bool initGL();
    void bouclePrincipale();


    private:

    std::string m_titreFenetre;
    int m_largeurFenetre;
    int m_hauteurFenetre;

    SDL_Window* m_fenetre;
    SDL_GLContext m_contexteOpenGL;

    Input m_input;

    void updateRotations(float *angleX, float *angleY);
};

#endif

