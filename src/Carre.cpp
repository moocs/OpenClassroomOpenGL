//
// Created by Antonin on 27/03/2018.
//

#include "Carre.h"

#include <utility>

Carre::Carre(int orientation, float taille, std::string const vertexShader, std::string fragmentShader)
        : m_shader(vertexShader, std::move(fragmentShader)), m_vboID(0), m_vaoID(0), m_tailleVerticesBytes(18 * sizeof(float)),
          m_tailleCouleursBytes(18 * sizeof(float))
{
    // Chargement du shader
    m_shader.charger();

    // Division du paramètre taille
    taille /= 2;

    // Vertices temporaires
    if(orientation == NORM_X)
    {
        float verticesTmp[] =
                {0.0, taille, taille,  0.0, taille, -taille,  0.0, -taille, taille,
                 0.0, -taille, -taille,  0.0, -taille, taille,  0.0, taille, -taille};

        for (int i(0); i < 18; i++)
            m_vertices[i] = verticesTmp[i];

        }
    else if (orientation == NORM_Y)
    {
        float verticesTmp[] =
                {taille, 0.0, taille,  taille, 0.0, -taille,  -taille, 0.0, taille,
                 -taille, 0.0, -taille,  -taille, 0.0, taille,  taille, 0.0, -taille};

        for (int i(0); i < 18; i++)
            m_vertices[i] = verticesTmp[i];
    }
    else
    {
        float verticesTmp[] =
                {taille, taille, 0.0,  taille, -taille, 0.0,  -taille, taille, 0.0,
                 -taille, -taille, 0.0,  -taille, taille, 0.0,  taille, -taille, 0.0};

        for (int i(0); i < 18; i++)
            m_vertices[i] = verticesTmp[i];
    }

    // Couleurs temporaires
    float couleursTmp[] =
            {1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0,           // Face 1
             1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0};           // Face 1

    for (int i(0); i < 18; i++) {
        m_couleurs[i] = couleursTmp[i];
    }
}


Carre::~Carre()
{
    // Destruction du VBO
    glDeleteBuffers(1, &m_vboID);

    // Destruction du VAO
    glDeleteVertexArrays(1, &m_vaoID);
}

void Carre::charger()
{
    if(glIsBuffer(m_vboID) == GL_TRUE)
        glDeleteBuffers(1, &m_vboID);

    // Génération de l'ID
    glGenBuffers(1, &m_vboID);

    // Verrouillage du VBO
    glBindBuffer(GL_ARRAY_BUFFER, m_vboID);

    // Allocation de la mémoire
    glBufferData(GL_ARRAY_BUFFER, m_tailleVerticesBytes + m_tailleCouleursBytes, nullptr, GL_STATIC_DRAW);

    // Transfert des données
    glBufferSubData(GL_ARRAY_BUFFER, 0, m_tailleVerticesBytes, m_vertices);
    glBufferSubData(GL_ARRAY_BUFFER, m_tailleVerticesBytes, m_tailleCouleursBytes, m_couleurs);

    // Déverrouillage de l'objet
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // ====== VAO ========

    if(glIsVertexArray(m_vaoID) == GL_TRUE)
        glDeleteVertexArrays(1, &m_vaoID);

    // Génération de l'ID du VAO
    glGenVertexArrays(1, &m_vaoID);

    // Verrouillage du VAO
    glBindVertexArray(m_vaoID);

    // Verrouillage du VBO
    glBindBuffer(GL_ARRAY_BUFFER, m_vboID);


    // On remplie puis on active le tableau Vertex Attrib 0
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(0);

    // On rentre les couleurs dans le tableau Vertex Attrib 1
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(m_tailleVerticesBytes));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Déverrouillage du VAO
    glBindVertexArray(0);
}

void Carre::afficher(glm::mat4 &projection, glm::mat4 &modelview)
{
    // Activation du shader
    glUseProgram(m_shader.getProgramID());

    // Verrouillage VAO
    glBindVertexArray(m_vaoID);

        // On envoie les matrices au shader
        glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "modelview"),
                           1, GL_FALSE, value_ptr(modelview));
        glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "projection"),
                           1, GL_FALSE, value_ptr(projection));

        // On affiche le triangle
        glDrawArrays(GL_TRIANGLES, 0, 6);

    // Déverrouillage VAO
    glBindVertexArray(0);

    // Désactivation du shader
    glUseProgram(0);

}

void Carre::updateBuffer(void *donnees, int tailleBytes, int decalage) {
    // Verrouillage du VBO
    glBindBuffer(GL_ARRAY_BUFFER, m_vboID);

    // Récupération de l'adresse du VBO
    void *adresseVBO = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

    // Si l'adresse retournée est nulle alors on arrête le transfert
    if(adresseVBO == nullptr)
    {
        std::cout << "Erreur au niveau de la récupération du VBO" << std::endl;
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        return;
    }

    // Mise à jour des données
    memcpy((char*)adresseVBO + decalage, donnees, static_cast<size_t>(tailleBytes));

    // Annulation du pointeur
    glUnmapBuffer(GL_ARRAY_BUFFER);
    adresseVBO = nullptr;

    // Déverrouillage du VBO
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}
