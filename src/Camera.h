//
// Created by Antonin on 27/03/2018.
//

#ifndef OPCL_CLION_CAMERA_H
#define OPCL_CLION_CAMERA_H

// Include

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Input.h"

using namespace glm;

class Camera
{
public:
    Camera();

    Camera(vec3 position, vec3 pointCible, vec3 axeVertical, float sensibilite, float vitesse);

    ~Camera();

    void orienter(int xRel, int yRel);

    void deplacer(Input const &input);

    void lookAt(mat4 &modelview);

    void setM_pointCible(const vec3 &m_pointCible);

    void setM_position(const vec3 &m_position);

    void setM_sensibilite(float m_sensibilite);

    void setM_vitesse(float m_vitesse);

    float getM_sensibilite() const;

    float getM_vitesse() const;

private:
    float m_phi;
    float m_theta;
    vec3 m_orientation;

    vec3 m_axeVertical;
    vec3 m_deplacementLateral;

    vec3 m_position;
    vec3 m_pointCible;

    float m_sensibilite;
    float m_vitesse;

    void updateOrientation();
};


#endif //OPCL_CLION_CAMERA_H
