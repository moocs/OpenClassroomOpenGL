//
// Created by Antonin on 27/03/2018.
//

#include "Camera.h"

Camera::Camera() : m_phi(0.0), m_theta(0.0), m_orientation(), m_axeVertical(0, 1, 0),
                   m_deplacementLateral(), m_position(), m_pointCible(), m_sensibilite(0.0), m_vitesse(0.0)
{

}

Camera::Camera(vec3 position, vec3 pointCible, vec3 axeVertical, float sensibilite, float vitesse) :
        m_phi(0.0), m_theta(0.0), m_orientation(),
        m_axeVertical(axeVertical),
        m_deplacementLateral(), m_position(position),
        m_pointCible(pointCible), m_sensibilite(sensibilite), m_vitesse(vitesse)
{

    setM_pointCible(pointCible);
}

void Camera::orienter(int xRel, int yRel)
{
    // Modification des angles
    m_phi += -yRel * m_sensibilite;
    m_theta += -xRel * m_sensibilite;

    //Limitation de phi
    if (m_phi > 89.0) {
        m_phi = 89.0;
    } else if (m_phi < -89.0) {
        m_phi = static_cast<float>(-89.0);
    }

    // Calcul des coordonnées sphériques
    updateOrientation();

    // Calcul de la normale
    m_deplacementLateral = cross(m_axeVertical, m_orientation);
    m_deplacementLateral = normalize(m_deplacementLateral);

    m_pointCible = m_position + m_orientation;
}

void Camera::updateOrientation()
{

    auto phiRadian = static_cast<float>(m_phi * M_PI / 180.0);
    auto thetaRadian = static_cast<float>(m_theta * M_PI / 180.0);

    // Si l'axe vertical est l'axe X
    if(m_axeVertical.x == 1.0)
    {
        // Calcul des coordonnées sphériques

        m_orientation.x = sin(phiRadian);
        m_orientation.y = cos(phiRadian) * cos(thetaRadian);
        m_orientation.z = cos(phiRadian) * sin(thetaRadian);
    }


    // Si c'est l'axe Y
    else if(m_axeVertical.y == 1.0)
    {
        // Calcul des coordonnées sphériques

        m_orientation.x = cos(phiRadian) * sin(thetaRadian);
        m_orientation.y = sin(phiRadian);
        m_orientation.z = cos(phiRadian) * cos(thetaRadian);
    }

    // Sinon c'est l'axe Z
    else
    {
        // Calcul des coordonnées sphériques

        m_orientation.x = cos(phiRadian) * cos(thetaRadian);
        m_orientation.y = cos(phiRadian) * sin(thetaRadian);
        m_orientation.z = sin(phiRadian);
    }
}

void Camera::deplacer(Input const &input)
{
    // Avancée de la caméra
    if(input.getTouche(SDL_SCANCODE_UP))
    {
        m_position = m_position + m_orientation * m_vitesse;
        m_pointCible = m_position + m_orientation;
    }

    // Recul de la caméra
    if(input.getTouche(SDL_SCANCODE_DOWN))
    {
        m_position = m_position - m_orientation * m_vitesse;
        m_pointCible = m_position + m_orientation;
    }

    // Déplacement vers la gauche
    if(input.getTouche(SDL_SCANCODE_LEFT))
    {
        m_position = m_position + m_deplacementLateral * m_vitesse;
        m_pointCible = m_position + m_orientation;
    }

    // Déplacement vers la droite
    if(input.getTouche(SDL_SCANCODE_RIGHT))
    {
        m_position = m_position - m_deplacementLateral * m_vitesse;
        m_pointCible = m_position + m_orientation;
    }

    // Gestion de l'orientation
    if(input.mouvementSouris())
        orienter(input.getM_xRel(), input.getM_yRel());
}

void Camera::setM_position(const vec3 &position)
{
    //Mise à jour de la position
    m_position = position;

    // Actualisation du point cible
    m_pointCible = m_pointCible + m_orientation;
}

void Camera::setM_pointCible(const vec3 &m_pointCible)
{

    // Calcul du vecteur orientation
    m_orientation = m_pointCible - m_position;
    m_orientation = normalize(m_orientation);

    // Si l'axe vertical est l'axe X
    if(m_axeVertical.x == 1.0)
    {
        // Calcul des angles
        m_phi = asin(m_orientation.x);
        m_theta = acos(m_orientation.y / cos(m_phi));

        if(m_orientation.y < 0)
            m_theta *= -1;
    }

    // Si c'est l'axe Y
    else if (m_axeVertical.y == 1.0) {
        // Calcul des angles
        m_phi = asin(m_orientation.y);
        m_theta = acos(m_orientation.z / cos(m_phi));

        if (m_orientation.z < 0)
            m_theta *= -1;
    }

    // Sinon c'est l'axe Z
    else {

        // Calcul des angles
        m_phi = asin(m_orientation.x);
        m_theta = acos(m_orientation.z / cos(m_phi));

        if (m_orientation.z < 0)
            m_theta *= -1;
    }

    // Conversion en degrés
    m_phi = static_cast<float>(m_phi * 180 / M_PI);
    m_theta = static_cast<float>(m_theta * 180 / M_PI);
}

void Camera::lookAt(mat4 &modelview)
{
    modelview = glm::lookAt(m_position, m_pointCible, m_axeVertical);
}

float Camera::getM_sensibilite() const {
    return m_sensibilite;
}

float Camera::getM_vitesse() const {
    return m_vitesse;
}

void Camera::setM_sensibilite(float m_sensibilite) {
    Camera::m_sensibilite = m_sensibilite;
}

void Camera::setM_vitesse(float m_vitesse) {
    Camera::m_vitesse = m_vitesse;
}

Camera::~Camera() = default;
