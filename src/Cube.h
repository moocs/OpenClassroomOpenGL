//
// Created by Antonin on 23/03/2018.
//

#ifndef OPCL_CLION_CUBE_H
#define OPCL_CLION_CUBE_H

// Includes OpenGL

#ifdef WIN32
#include <GL/glew.h>

#else
#define GL3_PROTOTYPES 1
#include <GL3/gl3.h>

#endif


// Includes GLM

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>


// Includes

#include "Shader.h"

// Macro utile au VBO

#ifndef BUFFER_OFFSET

#define BUFFER_OFFSET(offset) ((char*)NULL + (offset))

#endif

// Classe Cube

class Cube
{
public:

    Cube(float taille, std::string vertexShader, std::string fragmentShader);
    ~Cube();
    void afficher(glm::mat4 &projection, glm::mat4 &modelview);

    virtual void afficher(glm::mat4 &projection, glm::mat4 &modelview, glm::vec3 translation);


    virtual void charger();

    void updateVBO(void *donnees, int tailleBytes, int decalage);

protected:

    // Attributs
    Shader m_shader;
    float m_vertices[108];
    float m_couleurs[108];

    GLuint m_vboID;
    GLuint m_vaoID;
    int m_tailleVerticesBytes;
    int m_tailleCouleursBytes;

};

#endif //OPCL_CLION_CUBE_H
