//
// Created by Antonin on 26/03/2018.
//

#ifndef OPCL_CLION_TEXTURE_H
#define OPCL_CLION_TEXTURE_H

// Include

#ifdef WIN32
#include <GL/glew.h>

#else
#define GL3_PROTOTYPES 1
#include <GL3/gl3.h>

#endif

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <string>

class Texture {

public:
    explicit Texture(std::string fichierImage);

    Texture();

    Texture(Texture const &textureACopier);

    Texture(int largeur, int hauteur, GLenum format, GLenum formatInterne, bool textureVide);

    virtual ~Texture();

    Texture &operator=(Texture textureACopier);

    bool charger();

    void chargerTextureVide();

    GLuint getID() const;

    void setM_fichierImage(const std::string &m_fichierImage);

private:

    GLuint m_id;
    std::string m_fichierImage;

    int m_largeur;
    int m_hauteur;
    GLenum m_format;
    GLenum m_formatInterne;
    bool m_textureVide;

    SDL_Surface* inverserPixels(SDL_Surface *imageSource) const;
};


#endif //OPCL_CLION_TEXTURE_H
