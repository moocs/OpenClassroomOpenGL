// Version
#version 150 core

// Entrées
in vec3 in_Vertex;
in vec3 in_Color;

// Uniform
uniform mat4 modelview;
uniform mat4 projection;

//Sorties
out vec3 color;

// Foncrion main
void main()
{
    // Position finale du vertex
    gl_Position = projection * modelview * vec4(in_Vertex, 1.0);

    // Envoi de la couleur au fragment
    color = in_Color;

}