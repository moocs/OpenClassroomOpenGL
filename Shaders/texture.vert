// Version
#version 150 core

// Entrées
in vec3 in_Vertex;
in vec2 in_TexCoord0;

// Uniform
uniform mat4 modelviewProjection;

//Sorties
out vec2 coordTexture;

// Foncrion main
void main()
{
    // Position finale du vertex
    gl_Position = modelviewProjection * vec4(in_Vertex, 1.0);

    // Envoi de la couleur au fragment
    coordTexture = in_TexCoord0;
}