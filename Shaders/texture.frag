// Version
#version 150 core

// Entr�es
in vec2 coordTexture;

// Sorties
out vec4 out_Color;

// Uniform
uniform sampler2D texture;

// Fonction main
void main()
{
    // Recopie et inverse la couleur
    out_Color = texture(texture, coordTexture);
}